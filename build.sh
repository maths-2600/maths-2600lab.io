# https://www.cyberciti.biz/faq/unix-linux-bash-script-check-if-variable-is-empty/
if [ -z "$CI_COMMIT_TAG" ]
then
  echo "Building current version"
  poetry run mkdocs build --strict --verbose --site-dir public
else
  echo "Building version $CI_COMMIT_TAG"
  poetry run mkdocs build --strict --verbose --site-dir public/$CI_COMMIT_TAG
fi
