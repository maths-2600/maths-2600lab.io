## Session du 06/04/2022

Voici les liens pour la première session du 06/04/2022

- [Slides du cours (pdf)](assets/slides-session1.pdf)
- [Slides du cours (powerpoint)](assets/slides-session1.pptx)
- [integers.cpp](assets/integers.cpp)
- [floats.cpp](assets/floats.cpp)
