## Session du 28/06/2022 et 29/06/2022

Voici les liens pour les sessions du 28/06/2022 (Introduction à la cryptographie)

- [Slides du cours](https://c2ba-learning.gitlab.io/maths/maths-2600-slides-cryptographie/1)
- [Script python de base pour TP](https://gitlab.com/c2ba-learning/maths/maths-2600-slides-cryptographie/-/blob/main/public/tp.py)
