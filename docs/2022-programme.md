# Programme 2022

Voici une brève description du programme qui sera couvert. Celui ci est amené à évoluer.

## Session 1 (06/04/2022)

- Le concept de nombre
    - Définition et formalisation des nombres entiers
    - Opérations de base et récursivité
    - Propriétés, démonstractions et principe de récurrence
- La notation des nombres en base
    - La base 10
    - Exponentiation
    - Autres bases
    - Algorithmes de l'addition et de la multiplication
- Créer des nombres
    - Equations et solutions
    - Opérations inverses
    - Nombres entier négatifs
    - Nombres rationnels
    - Nombres réels
- Représentation informatique
    - Représentation binaires
    - Nombres entiers
    - Nombres flottants
- Algorithmique numérique (session pratique)
    - Implémentation de quelques algorithmes
    - Premières notions de complexité
- Projet: Arithmatoy
    - Implémentation en C des algorithmes d'addition, soustraction et multiplication

## Session 2 (Début Mai 2022)

- Algorithmique et complexité
- Classes de complexité, problème P=NP
- Logique formelle
- Théorie de la calculabilité
- Incomplétude, indécidabilité

## Session 3 (Début Juin 2022)

- Introduction à la cryptographie
- Introduction aux statistiques et probabilités
