# Projet: Arithmatoy

Vous trouverez le [code de base du projet sur Gitlab](https://gitlab.com/maths-2600/arithmatoy-base).

Le projet est a réaliser seul ou en binôme.

## Sujet

L'objectif de ce projet est d'implémenter en C les algorithmes d'addition, soustraction et multiplication vu à l'école primaire.

L'implémentation doit fonctionner dans n'importe quelle base de 2 à 36, utilisant les chiffres parmis: `0123456789abcdefghijklmnopqrstuvwxyz`. Par exemple, le nombre 35 peut s'écrire:

- `35` en base 10
- `100011` en base 2 
- `23` en base 16 (hexadécimal)
- `z` en base 36
- `10` en base 35

Le [repository de base](https://gitlab.com/maths-2600/arithmatoy-base) contient déjà un squelette de projet. Votre objectif est d'implémenter les fonction `arithmatoy_add`, `arithmatoy_sub` et `arithmatoy_mul` définies dans [arithmatoy.c](https://gitlab.com/maths-2600/arithmatoy-base/-/blob/main/src/arithmatoy.c).

A l'issue de votre implémentation, l'ensemble des tests du projet doivent passer.

Plusieurs fonctions utilitaires vous sont données pour faciliter l'implémentation.

Attention: Vous devez implémenter les algorithmes directement sur les chaines de caractère d'entrée des fonctions, pas convertir ces chaines en nombre pour faire le calcul. En particulier vos fonctions doivent gérer des nombres arbitrairement grands.

En plus de cela, si la variable `VERBOSE` vaut `1`, il faudra afficher des logs sur stderr indiquant les étapes intermédiaires des calculs (opérations sur chiffres et retenues). Les logs à afficher doivent avoir le même format que ceux que vous trouverez dans les fichiers sous [`tests/outputs/`](https://gitlab.com/maths-2600/arithmatoy-base/-/tree/main/tests/outputs). Ces fichiers ont été généré avec mon implémentation et vous pouvez utiliser les tests de non regression python du projet pour valider que votre programme produit les même logs.

## Mise en place

Vous aurez besoin à minima de Git, CMake et un compilateur C. La configuration conseillée est gcc sous Linux/Mac, Visual Studio sous Windows, mais vous êtes libre d'adapter à votre cas d'utilisation. Le projet est configuré pour l'editeur Visual Studio Code pour tout ce qui est configuration de debug et formattage automatique.

Voici les étapes à suivre:

- Forkez le projet gitlab (si vous travaillez en binôme choisissez un compte qui servira pour le rendu)
- Clonez le repo en local
- Sourcez le script bash: `source .devenv/bash_init.sh`. Ce fichier contient des commandes bash utilitaires.
- Lancez la commande `setup`. Cette commande va installer [`vcpkg`](https://vcpkg.io/en/index.html), compiler la lib de test [`cmocka`](https://cmocka.org/), et lancer `cmake`.
- Compilez en debug: `cmake_build_debug`
- Lancez les tests en debug: `ctest_debug`

Les tests devraient échouer, vous devez implémenter les fonctions pour les faire passer.

## Lancer les tests de non regression python

Ces tests lancent `arithmatoy-cli`, l'application en ligne de commande compilée par le projet. Le but des tests est de comparer l'output de votre programme avec des outputs de référence 

- Lancez la commande `cmake_install_debug`. Cette commande va compiler et installer des fichiers dans `.local/cmake/dist`.
- Créez un environnement virtuel python: `python -m venv .venv`
- Activez le: `source .venv/bin/activate` (`source .venv/scripts/activate` sous windows)
- Installez `pytest` dans le venv: `python -m pip install pytest`
- Executez pytest: `python -m pytest`

Ces tests peuvent être assez long à executer et devraient également échouer, vous devez implémenter les fonctions pour les faire passer. Cela implique d'implémenter le logging de manière à matcher mes outputs. Referrez vous aux fichiers de reference stockés dans [`tests/outputs/`](https://gitlab.com/maths-2600/arithmatoy-base/-/tree/main/tests/outputs) pour observer et reproduire le format des logs.

Vous pouvez lancer la commande `python -m pytest --force-regen` pour forcer le re-création des fichiers de sortie à partir de votre implémentation, et comparer les différences dans une UI affichant les diff Git.

## Livrable

Vous enverrez un mail à `laurent.noel.c2ba@gmail.com` au format suivant:

- Sujet: [maths-2600-arithmatoy] [nom1-prenom1] [nom2-prenom2]
- Contenu: Le lien vers votre fork du projet de base et le hash du commit pointant sur votre implémentation.

Le commit que vous referencerez devra passer les tests en CI/CD Gitlab (déjà implémenté dans le projet de base, échoue sur la branche main).
